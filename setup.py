import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="countdown-timer",
    version="0.0.3",
    author="Snowcode",
    description="A simple countdown",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitea.com/chopin42/countdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
